package g30125.badiu.raluca.l2.e6;
import java.util.Scanner;
public class Ex6 {
	static int factorial(int N)
	{if(N==1) return 1;
		else
		return N*factorial(N-1);
	}
	public static void main(String[] args) {
		System.out.println("N=");
		Scanner in = new Scanner(System.in);
		int N= in.nextInt();
		//non recursive
		System.out.println("a)non recursive\n");
		int n=N;
		int f=1;
		while(n>0)
		{
			f=f*n;
			n--;
			
		}
		System.out.println(N+"!="+f);
		//recursive
		System.out.println("b)recursive\n");
		System.out.println(N+"!="+factorial(N));
	}
}
