package g30125.badiu.raluca.l3.e5;
import becker.robots.*;
public class Ex5 {
	
	public static void main (String [] args) {
		  City prague = new City();
		 
		  Wall blockAve0 = new Wall(prague, 1, 1, Direction.NORTH);
	      Wall blockAve1 = new Wall(prague, 1, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(prague, 2, 1, Direction.WEST);
	      Wall blockAve3 = new Wall(prague, 2, 1, Direction.SOUTH);
	      Wall blockAve4 = new Wall(prague, 1, 2, Direction.SOUTH);
	      Wall blockAve5 = new Wall(prague, 1, 2, Direction.NORTH);
	      Wall blockAve6 = new Wall(prague, 1, 2, Direction.EAST);
	      
	      Robot karel = new Robot(prague, 1, 2, Direction.SOUTH);
	      
	      Thing parcel = new Thing(prague, 2, 2);
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      
	      karel.move();
	      
	      karel.turnLeft();
	    
	      
	      karel.move();
	      
	      karel.turnLeft();
	     
	      karel.move();
	      
	      karel.pickThing();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      
	      karel.move();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      
	      karel.move();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      
	      karel.move();
	      
	      karel.putThing();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	
	}

}
