package g30125.badiu.raluca.l4.e2;

public class MyPoint {
	//atribut
			private int x;
			private int y;
			//constructori
			MyPoint(){
				x=0;
				y=0;
			}
			 MyPoint(int x, int y){
				 
				 this.x = x;
				 this.y = y;
				 
			 }
			 //getter and setter
			 int get_x() {
				 return x;
			 }
			 void set_x(int x) {
				 this.x=x;
			 }
			 int get_y() {
				 return y;
			 }
			 void set_y(int y) {
				 this.y=y;
			 }
			 void setXY(int x, int y){ 
				 this.x = x;	 
				 this.y = y;
			 }
			 //metode
			 public String toString(){
			    return "("+this.x+", "+this.y+")";
			 }
			 double distance(int x, int y){
				         int xDiff = this.x - x;
				         int yDiff = this.y - y;
				         return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
			 }
			 double distance(MyPoint another){
				         int xDiff = this.x - another.x;
				         int yDiff = this.y - another.y;
				         return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
			 }

}
