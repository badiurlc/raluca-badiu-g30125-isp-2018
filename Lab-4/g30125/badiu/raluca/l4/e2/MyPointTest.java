package g30125.badiu.raluca.l4.e2;

import static org.junit.Assert.*;
import org.junit.Test;
public class MyPointTest {
	MyPoint p1 = new MyPoint();
	MyPoint p2 = new MyPoint();
	MyPoint p3 = new MyPoint(2, 3);
	@Test
	public void testXY() {
		p2.setXY(5, 7);
		assertEquals(p2.get_x(),5);
		assertEquals(p2.get_y(),7);
	}
	@Test
	public void testX() {
		p1.set_x(1);
		assertEquals(p1.get_x(),1);

	}
	@Test
	public void testY() {
		p1.set_y(3);
		assertEquals(p1.get_y(),3);

	}
	@Test 
	public void testDistance() {
		p2.setXY(-1,5);
		assertEquals(p2.distance(2,1),5,0.01);
	}


}
