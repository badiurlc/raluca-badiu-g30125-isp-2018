package g30125.badiu.raluca.l4.e3;

public class Circle {
	private double radius;
	private String color;
	Circle(){
		radius=1;
		color="red";
	}
	Circle(double radius){
		this.radius=radius;
	}
	double getRadius() {
		return this.radius;
	}
	double getArea() {
		return this.radius*this.radius*Math.PI;
	}
}
