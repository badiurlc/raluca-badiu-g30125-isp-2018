package g30125.badiu.raluca.l4.e3;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest {

	@Test
	public void test() {
		Circle c=new Circle(2);
		assertEquals(c.getArea(),4*Math.PI,0.01);
	}

}
