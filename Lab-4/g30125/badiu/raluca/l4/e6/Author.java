package g30125.badiu.raluca.l4.e6;

public class Author {
	protected String name;
	protected String email;
	protected char gender;
	Author(String name,String email, char gender){
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	String get_name()
	{
		return this.name;
	}
	String get_email()
	{
		return this.email;
	}
	
	char get_gender()
	{
		return this.gender;
	}

}
