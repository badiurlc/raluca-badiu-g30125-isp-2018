package g30125.badiu.raluca.l4.e7;

public class Cylinder extends Circle {
	double height=1.0;
	Cylinder(){
	super();
	}
	Cylinder(double radius){
		super(radius);
	}
	Cylinder(double radius,double height){
		super(radius);
		this.height=height;
	}
	double getHeight() {
		return this.height;
	}
	double getVolume() {
		return super.getArea()*this.height;
	}
	@Override
	double getArea() {
		return 2*Math.PI*getRadius()*(getRadius()+this.height);
	}
}
