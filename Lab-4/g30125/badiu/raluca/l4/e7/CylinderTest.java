package g30125.badiu.raluca.l4.e7;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {

	@Test
	public void test() {
		Cylinder c=new Cylinder(2,1);
		assertEquals(c.getVolume(),4*Math.PI,0.01);
	}

}
