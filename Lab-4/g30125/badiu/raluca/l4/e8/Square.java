package g30125.badiu.raluca.l4.e8;

public class Square extends Rectangle{
	private double side;
	Square(){
		side=1.0;
	}
    Square(double side) {
        super(side, side);


    }

    Square(double side, String color, boolean filled) {
        super(side, side, color, filled);


    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public String toString() {
        return "A Square with side="+this.side+", which is a subclass of "+super.toString();
    }
}


