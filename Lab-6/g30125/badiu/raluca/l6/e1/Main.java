package g30125.badiu.raluca.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.GREEN, 100,115,150,"1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.YELLOW, 100,300,50,"2",false);
        b1.addShape(s2);
        Shape s3= new Rectangle(Color.BLACK,200,200,60,100,"3",false);
        b1.addShape(s3);
        
        b1.delete("2");
        
      
    }
}
