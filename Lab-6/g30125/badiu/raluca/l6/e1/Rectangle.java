package g30125.badiu.raluca.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;


    public Rectangle(Color color, int length,int width,int x, int y,String id,boolean filled) {
        super(color,x,y,id,filled);
        this.length = length;
        this.width=width;
    
    }
    
    public int getLength() {
        return length;
    }
    
    
    public int getWidth() {
        return width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+ width+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,width);
        if(isFilled()==true)
        	g.fillRect(getX(), getY(), length, width);
        
    }
    
    
}