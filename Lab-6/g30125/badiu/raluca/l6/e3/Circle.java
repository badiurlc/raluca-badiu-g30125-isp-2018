package g30125.badiu.raluca.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape{

    private int radius,x,y;
    private Color color;
    String id;
    boolean filled;

    public Circle(Color color, int radius,int x,int y,String id,boolean filled) {
        this.color=color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.filled=filled;
        this.radius = radius;
  
    }

    public int getRadius() {
        return radius;
    }
   
    

    public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	@Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(isFilled()==true)
        	g.fillOval(getX(), getY(), radius, radius);
    }
    
 
}
