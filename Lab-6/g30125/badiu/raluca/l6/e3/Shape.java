package g30125.badiu.raluca.l6.e3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.*;

public interface Shape {

	public abstract void draw(Graphics g);
	public abstract String getId();

}