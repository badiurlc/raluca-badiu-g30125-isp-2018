package g30125.badiu.raluca.l6.e4;

import java.util.Arrays;

public class CharSequenceExample implements CharSequence{

	private char [] chars;
	
	public CharSequenceExample(char[] chars) {
		super();
		this.chars = chars;
	}
	

	@Override
	public char charAt(int id) {
		return chars[id];
	}

	@Override
	public int length() {
		return chars.length;
	}

	@Override
	public CharSequence subSequence(int arg0, int arg1) {
		char [] newc=Arrays.copyOfRange(chars, arg0, arg1);
		return new CharSequenceExample(newc); 
	}

}
