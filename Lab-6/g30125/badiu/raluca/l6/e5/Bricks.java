package g30125.badiu.raluca.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Bricks {
	 private Color color;
	    private int length,width;
	    boolean filled;
	
	 public Bricks(Color color, boolean filled,int length, int width) {
			this.color = color;
			this.filled = filled;
			this.length=length;
			this.width=width;
		}
	 
	 

	public Color getColor() {
		return color;
	}



	public void setColor(Color color) {
		this.color = color;
	}




	public boolean isFilled() {
		return filled;
	}



	public void setFilled(boolean filled) {
		this.filled = filled;
	}



	public int getLength() {
		return length;
	}



	public void setLength(int length) {
		this.length = length;
	}



	public int getWidth() {
		return width;
	}



	public void setWidth(int width) {
		this.width = width;
	}



	public abstract void draw(Graphics g);

}
