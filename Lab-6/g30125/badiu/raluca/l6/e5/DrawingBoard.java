package g30125.badiu.raluca.l6.e5;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    Bricks[] shapes = new Bricks[100];

    public DrawingBoard() {
        super();
        this.setTitle("~Pyramid~");
        this.setSize(500,500);
        this.setVisible(true);
    }

    public void addBricks(Bricks b1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = b1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }

    }
}