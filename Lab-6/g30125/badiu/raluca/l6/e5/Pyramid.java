package g30125.badiu.raluca.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

public class Pyramid extends Bricks{

	private int n,i,start1,start2,a,b,x,y;
	
	public Pyramid(Color color, int x, int y, boolean filled, int length, int width,int n) {
		super(color, filled, length, width);
		this.n=n;
		this.x=x;
		this.y=y;
	}


	public int getN() {
		return n;
	}




	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}


	@Override
	public void draw(Graphics g) {
		 g.setColor(getColor());
		 start1=getX();
		 start2=getY();
		 a=1;
		 b=1;
		 while(n-b>0||b==1) {
			 a=b;
			 setX(start1);
			 setY(start2);
			 while(a!=0) {
	        g.drawRect(getX(),getY(),getLength(),getWidth());
	        if(isFilled()==true)
	        	g.fillRect(getX(), getY(),getLength(),getWidth());
	        a--;
	        setX(getX()+getLength());
			 }
	        start1=start1-getLength()/2;
	        start2=start2+getWidth();
	       
	       n=n-b;
	       b++;
		 }
	}
	
	

}
