package g30125.badiu.raluca.l6.e6;

import java.awt.*;
import java.util.Random;

public class Circle extends Shape{

    private int radius;
    private int n;

    public Circle(Color color, int radius,int x,int y,boolean filled,int n) {
        super(color,x,y,filled);
        this.radius = radius;
        this.n=n;
  
    }

    public int getRadius() {
        return radius;
    }
  
    public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	@Override
    public void draw(Graphics g) {
		if(this.n>0) {
    	setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
            radius +=20;
            setX(getX()-10);
            setY(getY()-10);          
            this.n--;
            draw(g);
            
    	}
}
}