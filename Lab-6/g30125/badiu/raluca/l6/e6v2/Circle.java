package g30125.badiu.raluca.l6.e6v2;

import java.awt.*;
import java.util.Random;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int radius,int x,int y,boolean filled) {
        super(color,x,y,filled);
        this.radius = radius;

    }

    public int getRadius() {
        return radius;
    }
  
    public void setRadius(int radius) {
		this.radius = radius;
	}
	@Override
	public void draw(Graphics g) {
		
		
		drawCircle(g,getX(),getY(),radius);
		
	}
	
    public void drawCircle(Graphics g,int x,int y,int radius) {
		
	
    	setColor(getColor());
    	g.drawOval(x, y, radius, radius);
    	 if(radius > 2) {	
   
    	    drawCircle(g,x + radius/4+radius/2, y+radius/4, radius/2);
    	    drawCircle(g,x - radius/4, y+radius/4, radius/2);
    	    
    	 }
    	
    	 
            
    	
}
}