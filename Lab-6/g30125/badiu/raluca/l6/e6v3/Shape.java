package g30125.badiu.raluca.l6.e6v3;

import java.awt.Color;
import java.awt.Graphics;
public abstract class Shape {

    private Color color;
    private int x,y;
    boolean filled;
    public Shape(Color color,int x,int y,boolean filled) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.filled=filled;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public int getX() {
    	return x;
    }
    
    public int getY() {
    	return y;
    }
    public void setX(int x) {
    	this.x=x;
    }
    public void setY(int y) {
    	this.y=y;
    }
    public boolean isFilled() {
    	return filled;
    }

    public abstract void draw(Graphics g);
    
}

