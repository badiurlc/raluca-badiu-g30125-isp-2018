package g30125.badiu.raluca.l8.e3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Encrypt {
	public static void main(String [] args) throws IOException{
        System.out.println("Encrypt(e)/Decrypt(d)?");
        Scanner sc = new Scanner(System.in);
    	char i = sc.next().charAt(0);
    	String n=new String();
    	if(i=='e')
    	{BufferedReader in1 = new BufferedReader(new FileReader("file.enc"));
        String s, s1 = new String();
        while((s = in1.readLine())!= null)
          s1 += s + "\n";
    		for (int index = 0; index < s1.length();
    			index++) {
    	     char a = s1.charAt(index);
    	     if(a!='\n'&&a!=' ')
    	     a++;
    	     n+=a;
    	}
    		System.out.println(n);
    	}
    	else if(i=='d') 
    	{BufferedReader in1 = new BufferedReader(new FileReader("file.dec"));
        String s, s2 = new String();
        while((s = in1.readLine())!= null)
          s2 += s + "\n";
    		for (int index = 0; index < s2.length();
        			index++) {
        	     char a = s2.charAt(index);
        	     if(a!='\n'&&a!=' ')
        	    	 a--;
        	     n+=a;
    	}
    		System.out.println(n);
    	}
    	else System.out.println("Invalid Input");
        
	}
}
