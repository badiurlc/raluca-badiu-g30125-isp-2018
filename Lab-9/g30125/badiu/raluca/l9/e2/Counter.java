package g30125.badiu.raluca.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
public class Counter extends JFrame{
    JTextArea text;
    JButton button;
Counter(){
    setTitle("counter");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    init();
    setSize(250,250);
    setVisible(true);
}

public void init(){
    
   this.setLayout(null);

   text=new JTextArea();
   text.setBounds(10,100,100,150);
   button=new JButton("PUSH");
   button.setBounds(50, 50, 100, 20);
   button.addActionListener(new Buton());
   add(text);
   add(button);
}

public static void main(String[] args) {
   new Counter();
}
class Buton implements ActionListener{
private int counter=0;
    @Override
    public void actionPerformed(ActionEvent e) {
        Counter.this.text.setText("");
        Counter.this.text.append("Number:"+counter);
        counter++;
        
    }
}
}