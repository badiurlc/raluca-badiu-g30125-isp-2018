package g30125.badiu.raluca.l9.e3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class FileOpen extends JFrame{
    JTextField text;
    JButton button;
    JTextArea name;
    FileOpen(){
        setTitle("File");
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       init();
       setSize(200,250);
       setVisible(true);
    }
    public void init(){
        
        this.setLayout(null);
        int width=80;int height = 20;
        name= new JTextArea();
       name.setBounds(100, 50, width, height);
       text = new JTextField();
       text.setBounds(10,50,width, height);
        button=new JButton("PUSH File");
        button.setBounds(50, 100, 100, 50);
        button.addActionListener(new Buton());
        add(text);
        add(button);
        add(name);
    }
    public static void main(String[]args){
        new FileOpen();
    }
    class Buton implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                String s=FileOpen.this.text.getText();
                FileOpen.this.name.setText("");
                try{
                    Scanner in;
                    File f=new File(s);
                    if(f.exists()){
                        in=new Scanner(f);
                        while(in.hasNextLine()){
                            String line=in.nextLine();
                            FileOpen.this.name.append(line);
                        }
                        in.close();
                    }
                    else
                        FileOpen.this.name.append("Error");
                    
                }catch(Exception e2){
                    e2.printStackTrace();
                    
                }
                
                
            }
        }
}